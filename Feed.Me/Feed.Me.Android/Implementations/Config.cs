﻿[assembly: Xamarin.Forms.Dependency(typeof(Feed.Me.Droid.Implementations.Config))]
//constroi o assembly para ir ao partilhado, buscar o codigo e compila o dll (tipo add references)

namespace Feed.Me.Droid.Implementations
{
    using System;

    using SQLite.Net.Interop;
    using Interfaces;

    public class Config : IConfig

    {
        private string directoryDB;

        private ISQLitePlatform platform;

        public string DirectoryDB
        {
            get
            {
                //se o caminho nao existir
                if (string.IsNullOrEmpty(directoryDB))
                {
                    //vou criar o caminho para a pasta onde vai ficar a bd
                    directoryDB = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                }

                return directoryDB;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    //vejo qual e a plataforma onde ele esta(android neste caso)
                    platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                    
                }

                return platform;

            }
        }
    }
}