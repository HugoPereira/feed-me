﻿using System;
using System.Collections.Generic;
using System.Text;
using Feed.Me.ViewModels;

namespace Feed.Me.Infastructure
{
    public class InstanceLocator
    {
        #region Properties

        public MainViewModel Main { get; set; }

        #endregion

        #region Constructors

        public InstanceLocator()
        {
            //instancio a mainviewmodel e cria-a
            this.Main = new MainViewModel();
        }

        #endregion
    }
}
