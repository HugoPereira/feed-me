﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Feed.Me.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace Feed.Me.Services
{
    public class ApiServices
    {
        public async Task<Response> CheckConnection()
        {
            //detecta se nao ha internet
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "please see your interner config."
                };
            }

            //deteta se ha conexao e nao consegue ir a internet
            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

            if (!isReachable)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "please see your interner connection."
                };
            }



            //se der tudo responde positivo
            return new Response
            {
                IsSucess = true,
                Message = "Ok"
            };
        }


        /// <summary>
        /// apanho a lista de paises
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        //public async Task<Response> GetList<T>(string urlBase, string servicePrefix, string controller, string categorie)
        //{
        //    try
        //    {
        //        var client = new HttpClient();
        //        string key = "2c71dbc19b304fa2beda718b5082605f";

        //        client.BaseAddress = new Uri(urlBase);

        //        //junta o url com o prefixo
        //        var url = string.Format("{0}{1}?country=pt&language=pt&category={2}&apiKey={3}", servicePrefix, controller, categorie, key);

        //        //envia o url completo
        //        var response = await client.GetAsync(url);

        //        //RECEBE O JSON
        //        var result = await response.Content.ReadAsStringAsync();

        //        //se nao retornar
        //        if (!response.IsSuccessStatusCode)
        //        {
        //            return new Response
        //            {
        //                IsSucess = false,
        //                Message = result

        //            };
        //        }
        //        //recebe o objecto e desserializa
        //        var list = JsonConvert.DeserializeObject<List<T>>(result);

        //        return new Response
        //        {
        //            IsSucess = true,
        //            Message = "Ok",
        //            Result = list
        //        };
        //    }
        //    catch (Exception e)
        //    {
        //        return new Response
        //        {
        //            IsSucess = false,
        //            Message = e.Message
        //        };
        //    }
        //}

        public async Task<Response> GetList<T>(string urlBase, string servicePrefix, string controller, string category)
        {
            try
            {
                var client = new HttpClient();
                string key = "2c71dbc19b304fa2beda718b5082605f";

                client.BaseAddress = new Uri(urlBase);

                //junta o url com o prefixo
                var url = string.Format("{0}{1}?country=pt&language=pt&category={2}&apiKey={3}", servicePrefix, controller, category, key);

                //envia o url completo
                var response = await client.GetAsync(url);


                //RECEBE O JSON
                var result = await response.Content.ReadAsStringAsync();

                //se nao retornar
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result

                    };
                }
                //recebe o objecto e desserializa
                var list = JsonConvert.DeserializeObject<T>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

    }
}
