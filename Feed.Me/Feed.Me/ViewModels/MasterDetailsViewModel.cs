﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Feed.Me.Models;

namespace Feed.Me.ViewModels
{
    public class MasterDetailsViewModel: BaseViewModel
    {

        private ObservableCollection<CategorieItemViewModel> _categories;

        private List<Categories> Categories = MainViewModel.GetInstance().CategoriesList;

        private IEnumerable<CategorieItemViewModel> ToCategorieItemViewModel()
        {
            //cada pais converte num novo coutryviewmodel
            return Categories.Select(c => new CategorieItemViewModel
            {
                Id = c.Id,
                Name = c.Name,
                Translate = c.Translate
            });
        }

        public ObservableCollection<CategorieItemViewModel> CategoriesList
        {
            get { return this._categories; }

            set
            {
                SetValue(ref this._categories, value);
            }
        }

        public MasterDetailsViewModel()
        {
            
            LoadMaster();
        }

       

        private void LoadMaster()
        {
           this.CategoriesList = new ObservableCollection<CategorieItemViewModel>(this.ToCategorieItemViewModel());
            this.CategoriesList.OrderBy(x => x.Id);
        }
    }
}
