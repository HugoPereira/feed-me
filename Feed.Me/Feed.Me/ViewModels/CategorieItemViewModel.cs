﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;
using System.Windows.Input;
using Feed.Me.Models;
using Feed.Me.Views.MasterDetails;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;

namespace Feed.Me.ViewModels
{
    public class CategorieItemViewModel: Categories
    {
        public ICommand SelectCategorieCommand
        {
            get { return new RelayCommand(SelectCategorie); }
        }
        
        private async void SelectCategorie()
        {

            MainViewModel.GetInstance().SelectedCategorie = new DetailViewModel(this);

            App.MasterDetail.IsPresented = false;

            //await App.MasterDetail.Detail.Navigation.PushAsync( new DetailPage());
            App.MasterDetail.Detail = new NavigationPage(new DetailPage());

        }
    }
}
