﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Feed.Me.Models;
using Feed.Me.Services;
using Feed.Me.Views;

namespace Feed.Me.ViewModels
{
    public class MainViewModel
    {
        public ApiServices _apiService;

        public DataServices _dataService;

        public List<Categories> CategoriesList { get; set; }

        public News NewsList { get; set; }

        public List<Article> ArticleList { get; set; }
           
        public string Name { get; set; }

        #region ViewModel



        public MasterDetailsViewModel News { get; set; }

        public DetailViewModel SelectedCategorie { get; set; }

        public NewsViewModel SelectedNews { get; set; }




        #endregion

        public MainViewModel()
        {
            //digo que ela e igual a ela propria
            instance = this;

            CategoriesList = new List<Categories>();

            LoadCategories();

            this.News = new MasterDetailsViewModel();

            

            this.SelectedCategorie = new DetailViewModel(CategoriesList[0]);

            this.Name = CategoriesList[0].Translate;

            _apiService = new ApiServices();

            _dataService = new DataServices();

        }

        private void LoadCategories()
        {
            CategoriesList.Add(new Categories { Id = 0, Name = "General", Translate = "Geral"});
            CategoriesList.Add(new Categories { Id = 1, Name = "Entertainment", Translate = "Entretenimento"});
            CategoriesList.Add(new Categories { Id = 2, Name = "Business", Translate = "Negócios"});
            CategoriesList.Add(new Categories { Id = 3, Name = "Health", Translate = "Saúde e bem estar"});
            CategoriesList.Add(new Categories { Id = 4, Name = "Science", Translate = "Ciências"});
            CategoriesList.Add(new Categories { Id = 5, Name = "Sports", Translate = "Desporto"});
            CategoriesList.Add(new Categories { Id = 6, Name = "Technology", Translate = "Tecnologia"});
        }

        #region singleton
        //design pattern singleton

        //instancio o proprio objecto, estatico para ser visto em qq lado
        private static MainViewModel instance;

        //crio um metodo estatico, onde ele se envia a ele proprio
        public static MainViewModel GetInstance()
        {
            //se a instancia nao existe crio-a
            if (instance == null)
            {
                return new MainViewModel();
            }

            //senao retorno-a
            return instance;
        }


        #endregion
    }
}
