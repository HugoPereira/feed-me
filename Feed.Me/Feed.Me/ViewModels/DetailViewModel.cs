﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Input;
using Feed.Me.Models;
using Feed.Me.Services;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;

namespace Feed.Me.ViewModels
{
    public class DetailViewModel: BaseViewModel
    {
        #region services
        private ObservableCollection<ArticleItemViewModel> _articles;

        private ApiServices _apiService;


        #endregion

        #region atributes

        public Categories Categorie;
        
        private News _news;

        private bool _isRefreshing;

        
        #endregion

        #region command

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadNews); }

        }


        private IEnumerable<ArticleItemViewModel> ToArticleItemViewModels()
        {

            //cada pais converte num novo coutryviewmodel
            return MainViewModel.GetInstance().ArticleList.Select(n => new ArticleItemViewModel
            {
                Source = (Source)n?.Source,
                Author = n?.Author,
                //PublishedAt = DateTime.Parse(n?.PublishedAt).ToLocalTime().ToString("g"),
                PublishedAt = ParseDate(n?.PublishedAt),
                Url = n?.Url,
                UrlToImage = n?.UrlToImage,
                Title = n?.Title,
                Description = n?.Description,
                Content = n?.Content
            });
        }

        private string ParseDate(string publishedAt)
        {
            return DateTime.Parse(publishedAt).ToString("dd-MM-yyyy HH:mm");
        }




        //news api
        public ICommand GoNewsCommand
        {
            get
            {
                return new RelayCommand(GoNews);
            }
        }

        private async void GoNews()
        {
            string url = "https://newsapi.org";
            Device.OpenUri(new Uri(url));
        }


        #endregion

        #region properties

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }

            set
            {
                SetValue(ref this._isRefreshing, value);
            }
        }

        

        public News NewsList
        {
            get { return this._news; }

            set
            {
                SetValue(ref this._news, value);
            }
        }

        public ObservableCollection<ArticleItemViewModel> Articles
        {
            get { return _articles; }

            set { SetValue(ref this._articles, value); }
        }

       

        #endregion

        public DetailViewModel(Categories categorie)
        {
            this.Categorie = categorie;
            this._apiService = new ApiServices();
            MainViewModel.GetInstance().Name = Categorie.Translate;
            this.IsRefreshing = false;
            this.LoadNews();
        }
        
        //carrega as noticias
        private async void LoadNews()
        {
            this.IsRefreshing = true;
            //testar a conexao
            var connection = await this._apiService.CheckConnection();
            //se nao tiver conexao
            if (!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", connection.Message, "Ok");

                //volta a pagina atras
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            //versoes <= 5.0 da erro java security em sites https
            //var response = await this._apiService.GetList<News>("https://newsapi.org", "/v2/", "top-headlines", Categorie.Name);

            var response = await this._apiService.GetList<News>("http://newsapi.org", "/v2/", "top-headlines", Categorie.Name);

            if (!response.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    response.Message,
                    "Ok");

                return;
            }

            this.IsRefreshing = false;
            MainViewModel.GetInstance().NewsList = (News)response.Result;

            //converto de lista para ObservableCollection
            
            this.NewsList = (News)response.Result;

            MainViewModel.GetInstance().ArticleList =(List<Article>)this.NewsList?.Articles;

            this.Articles = new ObservableCollection<ArticleItemViewModel>(this.ToArticleItemViewModels());

            
        }
    }
}
