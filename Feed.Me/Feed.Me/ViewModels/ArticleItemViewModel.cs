﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Feed.Me.Models;
using Feed.Me.Views;
using Feed.Me.Views.MasterDetails;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;

namespace Feed.Me.ViewModels
{
    public class ArticleItemViewModel: Article
    {
        public ICommand SelectNewsCommand
        {
            get { return new RelayCommand(SelectNews); }
        }
        
        private async void SelectNews()
        {
            MainViewModel.GetInstance().SelectedNews = new NewsViewModel(this);

            //await Application.Current.MainPage.Navigation.PushAsync(new NewsPage());
            await App.MasterDetail.Detail.Navigation.PushAsync(new NewsPage());

        }
    }
}
