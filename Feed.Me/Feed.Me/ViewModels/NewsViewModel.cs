﻿using System;
using System.Collections.Generic;
using System.Text;
using Feed.Me.Models;
using Xamarin.Forms;

namespace Feed.Me.ViewModels
{
    public class NewsViewModel:BaseViewModel
    {
        public Article Article { get; set; }
        
        private bool _isRunning;
        
        public bool IsRunning
        {
            get { return this._isRunning; }

            set
            {
                SetValue(ref this._isRunning, value);
            }
        }

        
        public NewsViewModel(Article article)
        {

            this.Article = article;
        }

    }
}
