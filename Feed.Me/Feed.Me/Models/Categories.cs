﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Feed.Me.Models
{
    public class Categories
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Translate { get; set; }
    }
}
