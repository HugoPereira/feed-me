﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Feed.Me.Models
{
    public class MasterPageItem
    {
        public int Title { get; set; }

        public string Icon { get; set; }

        public Type TargetType { get; set; }
    }
}
