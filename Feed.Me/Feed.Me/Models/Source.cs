﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Feed.Me.Models
{
    public class Source
    {
        [JsonProperty(PropertyName = "id")]
        public object Id { get; set; } = null;

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; } = null;
    }
}
