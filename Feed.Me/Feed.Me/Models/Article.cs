﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Feed.Me.Models
{
    public class Article
    {
        [JsonProperty(PropertyName = "source")]
        public Source Source { get; set; }

        [JsonProperty(PropertyName = "author")]
        public string Author { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "urlToImage")]
        public string UrlToImage { get; set; }

        [JsonProperty(PropertyName = "publishedAt")]
        public string PublishedAt { get; set; }

        [JsonProperty(PropertyName = "content")]
        public string Content { get; set; }
    }
}
