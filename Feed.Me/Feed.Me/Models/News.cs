﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Feed.Me.Models
{
    public class News
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "totalResults")]
        public int TotalResults { get; set; }

        [JsonProperty(PropertyName = "articles")]
        public List<Article> Articles { get; set; }
    }
}
