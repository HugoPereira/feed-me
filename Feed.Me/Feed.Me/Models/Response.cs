﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Feed.Me.Models
{
    using System.Net;

    public class Response
    {
        public bool IsSucess { get; set; }

        public string Message { get; set; }

        public object Result { get; set; }

        public HttpStatusCode Code { get; set; }
    }
}
