﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feed.Me.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Feed.Me.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewsPage : ContentPage
	{
		public NewsPage ()
		{
			InitializeComponent ();
		}

        //protected async override void OnAppearing()
        //{
        //    base.OnAppearing();

        //    await ProgressB.ProgressTo(0.9, 900, Easing.SpringIn);
        //}

        private void WebView_OnNavigating(object sender, WebNavigatingEventArgs e)
	    {
	        WebView.IsEnabled = false;
	        WebView.IsVisible = false;
            //ProgressB.IsVisible = true;
	        Indicator.IsRunning = true;
	        Indicator.IsVisible = true;

	    }

	    private void WebView_OnNavigated(object sender, WebNavigatedEventArgs e)
	    {
	        WebView.IsEnabled = true;
	        //ProgressB.IsVisible = false;
	        Indicator.IsRunning = false;
	        Indicator.IsVisible = false;
	        WebView.IsVisible = true;
        }
	}
}