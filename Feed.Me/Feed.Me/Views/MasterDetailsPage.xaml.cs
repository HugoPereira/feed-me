﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feed.Me.Views.MasterDetails;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Feed.Me.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterDetailsPage : MasterDetailPage
	{
		public MasterDetailsPage ()
		{
			InitializeComponent ();

            this.Master = new MasterPage();
            this.Detail = new NavigationPage(new DetailPage());
		    

            //para conseguir navegar
            App.MasterDetail = this;
		}
	}
}