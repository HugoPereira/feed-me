﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Feed.Me.Interfaces
{
    using SQLite.Net.Interop;

    public interface IConfig
    {
        //caminho
        string DirectoryDB { get; }

        //diz qual e a plataforma onde esta a trabalhar
        ISQLitePlatform Platform { get; }
    }
}
