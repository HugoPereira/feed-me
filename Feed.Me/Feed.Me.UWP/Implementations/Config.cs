﻿[assembly: Xamarin.Forms.Dependency(typeof(Feed.Me.UWP.Implementations.Config))]
//constroi o assembly para ir ao partilhado, buscar o codigo e compila o dll (tipo add references)

namespace Feed.Me.UWP.Implementations
{
    using System;
    using Interfaces;
    using SQLite.Net.Interop;

    public class Config : IConfig
    {
        private string directoryDB;

        private ISQLitePlatform platform;

        public string DirectoryDB
        {
            get
            {
                if (string.IsNullOrEmpty(directoryDB))
                {
                    //vai buscar o caminho da pasta, onde fica armazenado
                    directoryDB = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                }

                return directoryDB;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    //vai buscar a plataforma(ios neste caso)
                    platform = new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT();
                }

                return platform;
            }
        }
    }
}