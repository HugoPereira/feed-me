﻿[assembly: Xamarin.Forms.Dependency(typeof(Feed.Me.iOS.Implementations.Config))]
//constroi o assembly para ir ao partilhado, buscar o codigo e compila o dll (tipo add references)

namespace Feed.Me.iOS.Implementations
{
    using System;
    using Interfaces;
    using SQLite.Net.Interop;

    public class Config : IConfig
    {
        private string directoryDB;

        private ISQLitePlatform platform;

        public string DirectoryDB
        {
            get
            {
                if (string.IsNullOrEmpty(directoryDB))
                {
                    //vai buscar o caminho da pasta, onde fica armazenado
                    var directory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    directoryDB = System.IO.Path.Combine(directory, "..", "Library");
                }

                return directoryDB;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    //vai buscar a plataforma(ios neste caso)
                    platform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();
                }

                return platform;
            }
        }
    }
}
